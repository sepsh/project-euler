def isprime(n):
    if n < 2:
        return False
    for i in range(2, int(n**0.5)+1):
        if n % i == 0:
            return False
    return True

c = 0
i = 0
while c < 10001:
    i += 1
    if isprime(i):
        c += 1

print(i)
