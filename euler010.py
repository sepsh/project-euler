def isprime(n):
    if n < 2:
        return False
    for i in range(2, int(n**0.5) + 1):
        if n % i == 0:
            return False
    return True


def sumofprimes(n):
    sum_of = 0
    for i in range(n):
        if isprime(i):
            sum_of += i
    return sum_of

print(sumofprimes(2000000))
