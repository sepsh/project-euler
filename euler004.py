big = 0
for a in range(100, 1000):
    for b in range(100, 1000):
        x = a * b
        if str(x) == str(x)[::-1] and x > big:
            big = x

print(big)
