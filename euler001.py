def check(n):
    if n % 3 == 0 or n % 5 == 0:
        return True
    return False

summ = 0
for i in range(1, 1000):
    if check(i):
        summ += i

print(summ)
